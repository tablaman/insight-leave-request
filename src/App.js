import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import logo from './logo.svg';
import './App.css';
import Home from './pages/Home';
import NewLeaveRequest from './pages/NewLeaveRequest';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      response: '',
      post: '',
      responseToPost: ''
    };
  }
  componentDidMount() {
    this.callApi()
      .then(res =>
        this.setState({
          response: res.express
        })
      )
      .catch(err => console.log(err));
  }
  callApi = async () => {
    const response = await fetch('/api/hello');
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };
  notify = () => {
    toast.success('🦄 Thank you for your submission!.');
  }
  handleSubmit = async e => {
    e.preventDefault();
    const response = await fetch('/api/world', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        post: this.state.post
      })
    });
    const body = await response.text();
    this.setState({
      responseToPost: body
    });
  };
  onSubmit = async e => {
    
    console.log('ONSUBMIT ', e);
    const response = await fetch('/api/world', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        post: e.reason
      })
    });
    const body = await response.text();
    this.setState({
      responseToPost: body
    });
  };
  handleOnChange = e => {
    e.preventDefault();
    console.log(e.target.value);
    this.setState({ post: e.target.value });
  };
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>
        <Router>
          <div>
            <nav>
              <ul>
                <li>
                  <Link to="/">Home</Link>
                </li>
                <li>
                  <Link to="/new-leave-request/">New Leave Request</Link>
                </li>
              </ul>
            </nav>

            <Route path="/" exact component={Home} />
            <Route path="/new-leave-request/" 
              render={props => <NewLeaveRequest onSubmit={this.onSubmit} />} />
          </div>
        </Router>
        <p> {this.state.response} </p>
        <form onSubmit={this.handleSubmit}>
          <p>
            <strong> Post to Server: </strong>
          </p>
          <input
            type="text"
            value={this.state.post}
            onChange={this.handleOnChange}
          />
          <button type="submit" onClick={this.notify}>
            
            Submit
          </button>
        </form>
        <ToastContainer />
        <p> {this.state.responseToPost} </p>
      </div>
    );
  }
}
export default App;
