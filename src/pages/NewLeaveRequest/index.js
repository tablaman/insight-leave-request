import React, { Component } from 'react';

class NewLeaveRequest extends Component {
  // We're going to localise state here
  // as there's no need to have the state object
  // passed down from App with events firing around
  // just to capture user entries.
  constructor(props) {
    super(props);
    this.state = {
      dateFrom: null,
      dateTo: null,
      reason: '',
      note: ''
    };
  }
  handleSubmit = e => {
    e.preventDefault();
    this.props.onSubmit(this.state);
  };
  handleOnChange = e => {
    e.preventDefault();
    this.setState({ reason: e.target.value });
  }
  render() {
    return (
      <div>
        <h1>New Leave Request</h1>
        <form onSubmit={this.handleSubmit}>
          <p>
            <strong> HELLO </strong>
          </p>
          <input type="text" value={this.state.reason} onChange={this.handleOnChange} />
          <button type="submit">
            Reason
          </button>
        </form>
      </div>
    );
  }
}

export default NewLeaveRequest;
